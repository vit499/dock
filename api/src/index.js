const express = require('express')

const app = express();

const port = process.env.DOCK_PORT || 6002;

app.use('/', (req, res) => {
    res.send("This is api server, ci/cd from gitlab")
})

app.listen(port, () => {
    console.log(`server started on port ${port}`)
})